scripted_gui = {
	ABY_AF_kattail_portrait_toggle = {
		context_type = player_context
		parent_window_token = politics_tab

		dirty = ABY_AF_kattail_portrait_dirty

		visible = {
			ROOT = {
				original_tag = ABY
				has_completed_focus = ABY_AF_are_worth_taking_over
				has_country_leader = { ruling_only = yes character = ABY_AF_kaiser_kattail }
				is_ai = no
			}
		}

		window_name = "ABY_AF_kattail_portrait_toggle_window"

		triggers = {
			ABY_AF_kattail_portrait_toggle_button_click_enabled = {
				ROOT = {
					NOT = { has_country_flag = ABY_AF_kattail_portrait_toggle_flag }
					is_ai = no
				}
			}
		}

		effects = {
			ABY_AF_kattail_portrait_toggle_button_click = {
				ROOT = {
					set_country_flag = ABY_AF_kattail_portrait_toggle_flag
					country_event = { id = katzen_AF.620 }
					set_variable = {
						ABY_AF_kattail_portrait_dirty = 0
					}
				}
			}
		}
	}

	ABY_AF_giant_tank_nuke_state = {
		context_type = selected_state_context
		window_name = "ABY_AF_giant_tank_nuke_state_window"
		parent_window_token = selected_state_view

		visible = {
			ROOT = { has_completed_focus = ABY_AF_giant_tank is_ai = no }
		}

		triggers = {
			ABY_AF_giant_tank_nuke_state_button_click_enabled = {
				hidden_trigger = {
					ABY = {
						check_variable = { num_battalions_with_type@aby_af_giant_tank = 1 }
						num_of_nukes > 9
					}
					controller = { has_war_with = ABY NOT = { tag = ZRS } }
				}
			}
		}

		effects = {
			ABY_AF_giant_tank_nuke_state_button_click = {
				ROOT = {
					add_nuclear_bombs = -10
					launch_nuke = { state = PREV use_nuke = no }
					launch_nuke = { state = PREV use_nuke = no }
					launch_nuke = { state = PREV use_nuke = no }
					launch_nuke = { state = PREV use_nuke = no }
					launch_nuke = { state = PREV use_nuke = no }
					launch_nuke = { state = PREV use_nuke = no }
					launch_nuke = { state = PREV use_nuke = no }
				}
				add_dynamic_modifier = {
					modifier = fallout_thermonuclear
					days = 360
				}
				controller = {
					save_current_manpower = yes
				}
				add_manpower = -100000
				controller = {
					restore_previous_manpower = yes
				}
				remove_building = { type = arms_factory level = 25 }
				remove_building = { type = dockyard level = 25 }
				remove_building = { type = industrial_complex level = 25 }
				remove_building = { type = infrastructure level = 5 }
				remove_building = { type = synthetic_refinery level = 10 }
				remove_building = { type = anti_air_building level = 10 }
				remove_building = { type = radar_station level = 10 }
				remove_building = { type = rocket_site level = 2 }
				remove_building = { type = nuclear_reactor level = 1 }
				remove_building = { type = air_base level = 10 }
				remove_building = { type = fuel_silo level = 10 }
				remove_building = { type = stronghold_network level = 1 }
				remove_building = { type = synthetic_crystal_refinery level = 1 }
				remove_building = { type = magical_quarry level = 1 }
				remove_building = { type = mega_gun_emplacement level = 1 }
				remove_building = { type = nuclear_reactor_heavy_water level = 1 }
				remove_building = { type = commercial_nuclear_reactor level = 1 }

				set_building_level = { 
					type = nuclear_facility 
					level = 0
					province = {
						all_provinces = yes
					}
				}
				set_building_level = { 
					type = naval_facility
					level = 0
					province = {
						all_provinces = yes
					}
				}
				set_building_level = { 
					type = air_facility 
					level = 0
					province = {
						all_provinces = yes
					}
				}
				set_building_level = { 
					type = land_facility 
					level = 0
					province = {
						all_provinces = yes
					}
				}
				set_building_level = { 
					type = magical_facility 
					level = 0
					province = {
						all_provinces = yes
					}
				}

				set_building_level = {
					type = bunker
					level = 0
					province = {
						all_provinces = yes
						limit_to_border = no
					}
				}
				set_building_level = {
					type = coastal_bunker
					level = 0
					province = {
						all_provinces = yes
						limit_to_border = no
					}
				}
				set_building_level = {
					type = rail_way
					level = 0
					province = {
						all_provinces = yes
						limit_to_border = no
					}
				}
				set_building_level = {
					type = naval_base
					level = 0
					province = {
						all_provinces = yes
						limit_to_border = no
					}
				}
				set_building_level = {
					type = supply_node
					level = 0
					province = {
						all_provinces = yes
						limit_to_border = no
					}
				}
				controller = {
					add_war_support = -0.20
					add_stability = -0.10
				}
				if = {
					limit = { ROOT = { NOT = { has_country_flag = fired_kontinentale_sturmer } } }
					ROOT = { set_country_flag = fired_kontinentale_sturmer }
					every_country = { country_event = { id = katzen_AF.29 days = 1 } }
				}
			}
		}
	}
}